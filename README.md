## Used tools
- Terraform v0.14.7 
- aws-cli 1.19.12
- ansible 2.10.5
- Docker 20.10.2

## Running locally

### 1 - Configure your AWS credentions
```bash
$ aws configure
AWS Access Key ID [None]: <Access Key ID>
AWS Secret Access Key [None]: <Secret Access Key>
Default region name [None]: us-east-1
```

### 2 - Login in your docker resgistry account
```bash
$ docker login
Authenticating with existing credentials...
Login Succeeded
```

### 3- build and publish
```bash
$ make build_docker IMAGE_NAME=<your image name>
```

### 4 - Create a S3 bucket to store Terraform remote state
```bash
$ make create_config
```

### 5 - Create infrastructure
```bash
$ make infra_up
```

### 6 - Deploy
```bash
$ make deploy IMAGE_NAME=<your image name>
```

## Running on gitlab CI
### 1 - Create a repository in your personal account on https://gitlab.com/

### 2 - Add the environment [variables](https://docs.gitlab.com/ee/ci/variables/) listed below
- AWS_DEFAULT_REGION
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY
- CI_REGISTRY(ex: registry.gitlab.com)
- CI_REGISTRY_IMAGE(ex: registry.gitlab.com/apecnascimento/demoapi)
- CI_REGISTRY_PASSWORD
- CI_REGISTRY_USER

### 3 - Push the project to master branch
### 4 - To destroy all infrastructure just run the pipeline steps "destroy-infra:  and "destroy-infra-state-config" manually in Gitlab CI


