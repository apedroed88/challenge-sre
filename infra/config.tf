terraform {
  backend "s3" {
    bucket = "terraform-state-challenge-sre"
    key    = "terraform"
    region = "us-east-1"
  }
}